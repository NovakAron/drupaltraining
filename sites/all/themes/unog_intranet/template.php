<?php

/**
 * @file template.php
 */

function unog_intranet_preprocess_html() {

  $google_font_urls = array(
    'http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700',
    'http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700',
    'http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700,600,800',
  );

  foreach ($google_font_urls as $url) {
    drupal_add_css($url, array(
      'type' => 'external',
    ));
  }
}

function unog_intranet_preprocess_page(&$variables) {
  $full_request_path = request_path();
  if ($full_request_path == 'fr') {
    $request_path = '';
  }
  else {
    $request_path = preg_replace('/^fr\//', '', $full_request_path);
  }
  $variables['language_switch_en'] = $GLOBALS['base_path'] . $request_path;
  $variables['language_switch_fr']= $GLOBALS['base_path'] . 'fr/' . $request_path;
}
