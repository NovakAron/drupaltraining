<!-- top nav -->
      <div class="navbar global-nav">
        <div class="navbar-inner">
          <a class="brand" href="<?php print $front_page; ?>">United Nations Intranet</a>
          <ul class="nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-home icon-2x"></i> Location <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Addis Ababa</a></li>
                <li><a href="#">Bangkok</a></li>
                <li><a href="#">Beirut</a></li>
                <li><a href="#">Geneva</a></li>
                <li><a href="#">Nairobi</a></li>
                <li><a href="#">New York</a></li>
                <li><a href="#">Santiago</a></li>
                <li><a href="#">Vienna</a></li>
              </ul>
            </li>
          </ul>

          <ul class="nav pull-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle language-select" data-toggle="dropdown">Language <b class="caret red"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<?php print $language_switch_en; ?>">English</a></li>
                <li><a href="<?php print $language_switch_fr; ?>">French</a></li>
              </ul>
            </li>

            <li class="top-region">
              <?php print render($page['header']); ?>
            </li>

          </ul>
        </div><!--/.navbar-inner -->
      </div><!--/.navbar -->
      <!-- /top nav -->

          <nav role="navigation">
            <?php if (!empty($primary_nav)): ?>
              <?php print render($primary_nav); ?>
            <?php endif; ?>
            <?php if (!empty($secondary_nav)): ?>
              <?php print render($secondary_nav); ?>
            <?php endif; ?>
            <?php if (!empty($page['navigation'])): ?>
              <?php print render($page['navigation']); ?>
            <?php endif; ?>
          </nav>

<div class="main-container container">

  <header role="banner" id="page-header">
    <?php if (!empty($site_slogan)): ?>
      <p class="lead"><?php print $site_slogan; ?></p>
    <?php endif; ?>

  </header> <!-- /#header -->

  <div class="row-fluid">

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="span3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>  

    <section class="<?php print _bootstrap_content_span($columns); ?>">  
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted hero-unit"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <div class="well"><?php print render($page['help']); ?></div>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="span3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

  </div>
</div>
<footer class="footer container">
  <?php print render($page['footer']); ?>
</footer>
